
export user="abdelfatahmaher"

# Docker image parameters
export appname=udacity-devops-capstone
export version=0.1

export hostport=80
export containerport=1212

# Create dockerpath
export dockerpath=$user/$appname