#!/usr/bin/env bash
# Load configurations
. ./scripts/config.sh

# Build image
docker build --tag=$dockerpath:$version .

# Show exist images
docker images ls

# Delete local exit image.


# Run flask app
docker run -p $hostport:$containerport --name=$appname $dockerpath:$version

# Show exist containers
docker ps