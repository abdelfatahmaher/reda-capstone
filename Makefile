lint:
	#pylint --disable=R,C,W1203 flask/run.py flask/app/__init__.py flask/app/views.py
	#pylint --disable=R,C,W1203,W1202,W0311 app.py
	pylint --disable=R,C,W1203,W1202,W0311 flask/app/views.py


build:
	sh ./scripts/build_run_image.sh

upload:
	sh ./scripts/upload_image.sh