# Udacity DevOps Nanodegree Capstone Project
---

## 1. Test application locally.
   * Prepare python virtual environment.
   
```
    cd flask/
    python3 -m venv venv
    source venv/bin/activate
    pip install flask uwsgi
    pip install --upgrade pip
    pip freeze > requirements.txt
```
   * Run application locally.
 
```
    export FLASK_APP=run.py
    export FLASK_ENV=development
    flask run --host=0.0.0.0
```

## 2. Build application container image.
   The image is composed from two containers, which is built using docker-compose file.
    
   * python (flask) web-api: : listens to port [8080]..
   * nginx (webserver): listens to port [1212].
    
        > sudo docker-compose build --tag abdelfatahmaher/flask-capstone:v1.0
    
   Check that the image has been built successfully.
   
        > docker image ls
        
   Run the image within container to check the image works as expected.
        > docker run --name udacity-capstone  abdelfatahmaher/flask-capstone:v1.0

   
---
Resources
* [Flask API](https://pythonise.com/series/learning-flask/building-a-flask-app-with-docker-compose)
* [kubernetes](https://google.qwiklabs.com/focuses/1104?parent=catalog&qlcampaign=77-18-gcpd-236)